<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Pega o usuário logado
Auth::routes();
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('eventos', 'EventosController');
Route::post('/eventos/buscar', 'EventosController@buscar');
Route::post('/cadastro', ['uses' => 'UserController@create']);

//Route::resource('usuarios', 'UserController')->middleware('auth:api');

Route::group(['middleware' => ['auth:api']], function(){
    Route::get('usuario', 'UserController@index');
    Route::post('/user/salvarNome', 'UserController@salvarNome');
    Route::post('/user/trocaSenha', 'UserController@changePassword');
    Route::get('/user/eventos', 'EventosController@meusEventos');

    Route::post('/eventos/addBanner/{id}', 'EventosController@addBanner');

    Route::get('/eventos/participantes/{id}', 'EventosController@getParticipantes');

    Route::post('/eventos/inscrever', 'EventosController@inscrever');

    Route::post('/eventos/check_inscricao/{id}', 'EventosController@checkInscricao');
    Route::post('/eventos/excluir_inscricao', 'EventosController@excluirInscricao');

    Route::post('/eventos/envia_email', 'EventosController@enviaEmail');
});
