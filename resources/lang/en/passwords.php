<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'As senhas devem conter no minimo 6 caracteres.',
    'reset' => 'Sua senha foi alterada!',
    'sent' => 'Você receberá um link para resetar a senha em breve!',
    'token' => 'O token para resetar está expirado.',
    'user' => "Este usuário não possue cadastro.",

];
