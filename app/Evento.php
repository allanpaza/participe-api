<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Evento extends Model
{
  use Notifiable;

  protected $fillable = [
    'nome',
    'data_inicio',
    'data_fim',
    'presencial',
    'online',
    'pagina',
    'local',
    'ins_inicio',
    'ins_fim',
    'banner',
    'descricao',
    'user_id',
  ];

  protected $table = 'eventos';

  public function user()
  {
      return $this->belongsTo(User::class);
  }
}
