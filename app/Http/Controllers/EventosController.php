<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Mail\SendMailFormulario;
use App\Participacao;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Evento::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();

        return \App\Evento::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evento = Evento::find($id);
        return response()->json($evento);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dados = $request->all();
        $registro = Evento::find($id);
        $registro->update($dados);
        return response()->json($dados, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $evento = Evento::where([
            ['id', '=', $id],
        ])->delete();

    }

    public function meusEventos()
    {
        return $eventos = Evento::
        join('participacoes', 'eventos.id', '=', 'participacoes.evento_id')
            ->where('participacoes.user_id', Auth::user()->id)
            ->get();
    }

    public function addBanner(Request $request, $id)
    {
        $this->validate($request, [
            'banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $banner = $request->file('banner');
        if (!is_null($banner)) {
            $input['imagename'] = time() . '.' . $banner->getClientOriginalExtension();

            $destinationPath = public_path('/images/eventos/' . $id);

            $banner->move($destinationPath, $input['imagename']);

            $dados['banner'] = $input['imagename'];
        }

        $registro = Evento::find($id);

        return response()->json($registro->update($dados));
    }

    public function inscrever(Request $request)
    {
        //Verifica se já não tem inscrição nesse evento
        $this->validate($request, [
            'evento_id' => 'required|integer',

        ]);
        $dados = $request->all();
        $dados['user_id'] = Auth::user()->id;

        $participacao = Participacao::where([
            ['evento_id', '=', $dados['evento_id']],
            ['user_id', '=', $dados['user_id']],
        ])->get();
        if (sizeof($participacao) < 1)
            return Participacao::create($dados);
        else
            return response()->json('Usuário já está inscrito neste evento.');
    }

    public function buscar(Request $request)
    {
        $dados = $request->all();

        $eventos = Evento::
        where('nome', 'like', '%' . $dados['nome'] . '%')
            ->orWhere('descricao', 'like', '%' . $dados['nome'] . '%')
            ->get();
        if (!is_null($eventos))
            return response()->json($eventos);
        else
            return response()->json('Nenhum evento encontrado');
    }


    public function checkInscricao($id)
    {
        //Verifica se tem inscrição nesse evento
        Auth::user()->id;

        $participacao = Participacao::where([
            ['evento_id', '=', $id],
            ['user_id', '=', Auth::user()->id],
        ])->get();
        if (sizeof($participacao) == 1)
            return response()->json('true');
        else
            return response()->json('false');
    }

    public function getParticipantes($id)
    {
        //Retorno lista de participantes no evento
        return $participantes = User::
        join('participacoes', 'users.id', '=', 'participacoes.user_id')
            ->where('participacoes.evento_id', $id)
            ->orderBy('users.nome', 'asc')
            ->get();
    }

    public function excluirInscricao(Request $request)
    {
        //Pega os dados do post
        $dados = $request->all();

        return $participacao = Participacao::where([
            ['evento_id', '=', $dados['evento_id']],
            ['user_id', '=', $dados['user_id']],
        ])->delete();

    }


    public function enviaEmail(Request $request)
    {
        $dados = $request->all();

        $files = $request->file('arquivos');


        foreach ($files as $i => $arquivo) {
            if ($arquivo && $arquivo->isValid()) {
                // Define um aleatório para o arquivo baseado no timestamps atual
                $name = uniqid(date('HisYmd'));

                // Recupera a extensão do arquivo
                $extension = $arquivo->extension();

                // Define finalmente o nome
                $nameFile = "{$name}.{$extension}";

                $upload[] = $arquivo->storeAs('arquivos_inscricao', $nameFile);
            }
        }

        $pdf = PDF::loadView('email.formulario_inscricao', compact('dados'))->setPaper('a4', 'portrait')->setWarnings(false)->stream();

        Mail::to('INSCRICOES@FUNAEPE.ORG.BR')->send(new SendMailFormulario($dados, $pdf, $upload));

        foreach($upload as $arquivo_delete)
            Storage::delete($arquivo_delete);
    }
}
