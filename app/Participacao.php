<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Participacao extends Model
{
     use Notifiable;

     protected $table = 'participacoes';
     protected $fillable = [
        'user_id', 'evento_id',
    ];

     public function evento()
     {
         return $this->belongsTo(Evento::class);
     }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
