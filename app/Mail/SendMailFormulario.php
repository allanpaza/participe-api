<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailFormulario extends Mailable
{
    use Queueable, SerializesModels;
    public $formulario;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form, $ficha_inscricao, $arquivos = [])
    {
        $this->formulario = $form;
        $this->ficha_inscricao = $ficha_inscricao;
        $this->arquivos = $arquivos;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->attachData($this->ficha_inscricao, 'ficha_inscricao.pdf', [
            'mime' => 'application/pdf',
        ]);

        foreach ($this->arquivos as $key => $arquivo) {
            $email->attachFromStorage($arquivo);
        }

        return $email->from('nao-responda@webfundacao.com.br')
            ->subject('FORMULÁRIO DE INSCRIÇÃO')
            ->with(['dados' => $this->formulario])
            ->view('email.formulario_inscricao')
            ->attachData($this->ficha_inscricao, 'ficha_inscricao.pdf', [
                'mime' => 'application/pdf',
            ]);
        #json_encode($this);
        #exit(0);
    }
}
