<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::Class)->create([
            'nome' => 'Admin',
            'email' => 'allanpaza@gmail.com'
        ]);
    }
}
