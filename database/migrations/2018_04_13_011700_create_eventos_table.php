<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->boolean('presencial')->default(true);
            $table->boolean('online')->default(false);
            $table->string('pagina')->nullable();
            $table->string('local');
            $table->date('ins_inicio');
            $table->date('ins_fim');
            $table->string('banner')->nullable();
            $table->longText('descricao');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references("id")->on("users")->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
